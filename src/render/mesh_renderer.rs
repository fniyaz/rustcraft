use std::sync::Arc;

use vulkano::{
    buffer::{cpu_pool::CpuBufferPoolSubbuffer, BufferUsage, CpuBufferPool},
    command_buffer::{AutoCommandBufferBuilder, DynamicState, PrimaryAutoCommandBuffer},
    descriptor::descriptor_set::PersistentDescriptorSet,
    device::Device,
    image::view::ImageView,
    memory::pool::StdMemoryPool,
    render_pass::RenderPass,
    sampler::{Filter, MipmapMode, Sampler, SamplerAddressMode},
};

use super::{
    builtin_shaders::{instanced_vs, vs},
    camera::{Camera, CameraData},
    pipelines::{InstancedMeshPipeline, StaticMeshPipeline},
    static_mesh::{InstancedMesh, StaticMesh},
};

pub trait MeshRenderer<Mesh> {
    fn render_mesh<'b>(
        &self,
        command_buffer_builder: &'b mut AutoCommandBufferBuilder<PrimaryAutoCommandBuffer>,
        mesh: &Mesh,
    ) -> &'b mut AutoCommandBufferBuilder<PrimaryAutoCommandBuffer>;

    fn update_camera_data(&mut self, camera: CameraData);

    fn recreate_pipeline(
        &mut self,
        device: Arc<Device>,
        render_pass: Arc<RenderPass>,
        dimensions: &[u32; 2],
    );
}

struct MeshRendererBase<UniformData> {
    camera: CameraData,
    uniform_buffer: CpuBufferPool<UniformData>,
}

impl<UniformData> MeshRendererBase<UniformData> {
    fn get_mvp_matrix(
        &self,
        uniform_data: UniformData,
    ) -> CpuBufferPoolSubbuffer<UniformData, Arc<StdMemoryPool>> {
        self.uniform_buffer.next(uniform_data).unwrap()
    }
}

pub struct StaticMeshRenderer {
    base: MeshRendererBase<vs::ty::Data>,
    pipeline: StaticMeshPipeline,
}

impl StaticMeshRenderer {
    pub fn make(
        device: Arc<Device>,
        camera: CameraData,
        pipeline: StaticMeshPipeline,
    ) -> StaticMeshRenderer {
        let uniform_buffer =
            CpuBufferPool::<vs::ty::Data>::new(device.clone(), BufferUsage::uniform_buffer());

        StaticMeshRenderer {
            base: MeshRendererBase {
                uniform_buffer,
                camera,
            },
            pipeline,
        }
    }
}

impl MeshRenderer<StaticMesh> for StaticMeshRenderer {
    fn render_mesh<'b>(
        &self,
        command_buffer_builder: &'b mut AutoCommandBufferBuilder<PrimaryAutoCommandBuffer>,
        mesh: &StaticMesh,
    ) -> &'b mut AutoCommandBufferBuilder<PrimaryAutoCommandBuffer> {
        let layout = self
            .pipeline
            .get_vk_layout()
            .descriptor_set_layout(0)
            .unwrap();

        let (view, proj) = self.base.camera.get_view_projection_matrix();
        let subbuffer = self.base.get_mvp_matrix(vs::ty::Data {
            world: mesh.get_transform().into(),
            proj: proj.into(),
            view: view.into(),
        });
        let set = Arc::new(
            PersistentDescriptorSet::start(layout.clone())
                .add_buffer(subbuffer)
                .unwrap()
                .build()
                .unwrap(),
        );
        command_buffer_builder
            .draw(
                self.pipeline.get_vk_pipeline(),
                &DynamicState::none(),
                vec![mesh.vertex_buffer.clone()],
                set.clone(),
                (),
                vec![],
            )
            .unwrap()
    }

    fn update_camera_data(&mut self, camera: CameraData) {
        self.base.camera = camera;
    }

    fn recreate_pipeline(
        &mut self,
        device: Arc<Device>,
        render_pass: Arc<RenderPass>,
        dimensions: &[u32; 2],
    ) {
        self.pipeline
            .recreate(device.clone(), render_pass.clone(), &dimensions);
    }
}

pub struct InstancedMeshRenderer {
    base: MeshRendererBase<instanced_vs::ty::Data>,
    pipeline: InstancedMeshPipeline,
    sampler: Arc<Sampler>,
}

impl InstancedMeshRenderer {
    pub fn make(
        device: Arc<Device>,
        camera: CameraData,
        pipeline: InstancedMeshPipeline,
    ) -> InstancedMeshRenderer {
        let uniform_buffer = CpuBufferPool::<instanced_vs::ty::Data>::new(
            device.clone(),
            BufferUsage::uniform_buffer(),
        );
        let sampler = Sampler::new(
            device.clone(),
            Filter::Linear,
            Filter::Linear,
            MipmapMode::Nearest,
            SamplerAddressMode::ClampToEdge,
            SamplerAddressMode::ClampToEdge,
            SamplerAddressMode::ClampToEdge,
            0.0,
            1.0,
            0.0,
            0.0,
        )
        .unwrap();

        InstancedMeshRenderer {
            base: MeshRendererBase {
                uniform_buffer,
                camera,
            },
            pipeline,
            sampler,
        }
    }
}

impl MeshRenderer<InstancedMesh> for InstancedMeshRenderer {
    fn render_mesh<'b>(
        &self,
        command_buffer_builder: &'b mut AutoCommandBufferBuilder<PrimaryAutoCommandBuffer>,
        mesh: &InstancedMesh,
    ) -> &'b mut AutoCommandBufferBuilder<PrimaryAutoCommandBuffer> {
        let (view, proj) = self.base.camera.get_view_projection_matrix();
        let subbuffer = self.base.get_mvp_matrix(instanced_vs::ty::Data {
            world: mesh.get_transform().into(),
            proj: proj.into(),
            view: view.into(),
        });

        let mvp_layout = self
            .pipeline
            .get_vk_layout()
            .descriptor_set_layout(0)
            .unwrap();
        let mvp_set = Arc::new(
            PersistentDescriptorSet::start(mvp_layout.clone())
                .add_buffer(subbuffer)
                .unwrap()
                .build()
                .unwrap(),
        );

        let tex_layout = self
            .pipeline
            .get_vk_layout()
            .descriptor_set_layout(1)
            .unwrap();
        let tex_set = Arc::new(
            PersistentDescriptorSet::start(tex_layout.clone())
                .add_sampled_image(
                    ImageView::new(mesh.texture.clone()).unwrap(),
                    self.sampler.clone(),
                )
                .unwrap()
                .build()
                .unwrap(),
        );

        command_buffer_builder
            .draw(
                self.pipeline.get_vk_pipeline(),
                &DynamicState::none(),
                vec![
                    mesh.vertex_buffer.clone(),
                    mesh.instance_data_buffer.clone(),
                ],
                (mvp_set.clone(), tex_set.clone()),
                (),
                vec![],
            )
            .unwrap()
    }

    fn update_camera_data(&mut self, camera: CameraData) {
        self.base.camera = camera;
    }

    fn recreate_pipeline(
        &mut self,
        device: Arc<Device>,
        render_pass: Arc<RenderPass>,
        dimensions: &[u32; 2],
    ) {
        self.pipeline
            .recreate(device.clone(), render_pass.clone(), dimensions)
    }
}
