use std::path::Path;
use std::cell::RefCell;
use std::iter::FromIterator;
use std::rc::Rc;

use cgmath::{Matrix4, Vector3};
use vulkano::device::{Device, DeviceExtensions};
use vulkano::instance::{Instance, PhysicalDevice};
use vulkano::Version;
use vulkano_win::VkSurfaceBuild;
use winit::event::{ElementState, Event, WindowEvent};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::{WindowBuilder};

use super::Scene;
use super::renderer::RendererData;
use super::renderer::Renderer;

pub struct VulkanApplication {
    event_loop: Option<EventLoop<()>>,
    renderer: RendererData,
    scene: Rc<RefCell<Scene>>,
}

impl VulkanApplication {
    pub fn create() -> VulkanApplication {
        let required_extensions = vulkano_win::required_extensions();
        println!(
            "Available layers: {:?}",
            vulkano::instance::layers_list()
                .unwrap()
                .map(|prop| prop.name().to_owned())
                .collect::<Vec<_>>()
        );
        let mut desired_layers = std::collections::HashSet::<&str>::from_iter(vec![
            "VK_LAYER_KHRONOS_validation",
            "VK_LAYER_LUNARG_standard_validation",
            "VK_LAYER_NV_optimus",
        ]);
        let mut found_layers = Vec::<String>::new();

        for layer in vulkano::instance::layers_list().unwrap() {
            if desired_layers.contains(layer.name()) {
                found_layers.push(layer.name().to_owned());
                desired_layers.remove(layer.name());
            }
        }
        println!("Chosen layers: {:?}", found_layers);

        let instance = Instance::new(
            None,
            Version::V1_2,
            &required_extensions,
            found_layers.iter().map(|l| l.as_str()),
        )
        .unwrap();

        let event_loop = EventLoop::new();
        let surface = WindowBuilder::new()
            .build_vk_surface(&event_loop, instance.clone())
            .unwrap();

        PhysicalDevice::enumerate(&instance).for_each(|device| {
            println!(
                "Found device: {} (type: {:?})",
                device.properties().device_name.as_ref().unwrap(),
                device.properties().device_type.unwrap(),
            );
        });
        let physical = PhysicalDevice::enumerate(&instance).next().unwrap();
        println!(
            "Using device: {} (type: {:?})",
            physical.properties().device_name.as_ref().unwrap(),
            physical.properties().device_type.unwrap(),
        );
        let queue_family = physical
            .queue_families()
            .find(|&q| q.supports_graphics() && surface.is_supported(q).unwrap_or(false))
            .unwrap();
        let device_ext = DeviceExtensions {
            khr_swapchain: true,
            ..DeviceExtensions::none()
        };
        let (device, mut queues) = Device::new(
            physical,
            physical.supported_features(),
            &device_ext,
            [(queue_family, 0.5)].iter().cloned(),
        )
        .unwrap();
        let queue = queues.next().unwrap();

        let scene = Rc::new(RefCell::new(Scene::create(
            device.clone(),
            queue.clone(),
        )));
        Self::make_test_scene(scene.clone());
    
        let renderer = super::renderer::RendererData::create(
            surface.capabilities(physical).unwrap(),
            device.clone(),
            queue.clone(),
            surface.clone(),
            scene.clone(),
        );

        VulkanApplication {
            event_loop: Some(event_loop),
            scene,
            renderer
        }
    }

    fn make_test_scene(scene: Rc<RefCell<Scene>>) {
        let cat_obj_set = Scene::download_obj_set(Path::new("data/lowpolycat/cat.obj"));
        let cat = cat_obj_set.objects[1].clone();
        /*scene.borrow_mut().add_object(
            &cat,
            Matrix4::from_translation(Vector3::new(0f32, 0f32, 0f32)),
        );*/
        let cube_obj_set = Scene::download_obj_set(Path::new("data/cube.obj"));
        let cube = cube_obj_set.objects[0].clone();
    
        let mut cube_transforms = Vec::<Matrix4<f32>>::new();
        for x in 0..(16 * 3) {
            for y in 0..256 {
                for z in 0..(16 * 3) {
                    let transform = Matrix4::from_translation(Vector3::new(
                        x as f32 * 5f32,
                        -128f32 + y as f32 * 5f32,
                        z as f32 * 5f32,
                    ));
                    cube_transforms.push(transform);
                }
            }
        }
    
        let reader = std::io::BufReader::new(std::fs::File::open("data/Grass.png").unwrap());
        scene
            .borrow_mut()
            .add_instanced_object(&cube, cube_transforms, reader);    
    }

    pub fn run(mut self) -> ! {
        self.event_loop.take().unwrap()
            .run(move |event, _, control_flow| match event {
                Event::WindowEvent {
                    event: WindowEvent::CloseRequested,
                    ..
                } => {
                    *control_flow = ControlFlow::Exit;
                }
                Event::WindowEvent {
                    event: WindowEvent::Resized(_),
                    ..
                } => {
                    self.renderer.resize();
                }
                Event::RedrawEventsCleared => {
                    self.renderer.update();
                }
                Event::WindowEvent {
                    event: WindowEvent::KeyboardInput { input, .. },
                    ..
                } => {
                    if input.state == ElementState::Pressed {
                        self.renderer.on_pressed(&input);
                    } else if input.state == ElementState::Released {
                        self.renderer.on_released(&input);
                    }
                }
                _ => (),
            })
    }
}
