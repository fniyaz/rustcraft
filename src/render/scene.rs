use std::fs::File;
use std::path::Path;
use std::io::Read;
use std::{
    io::{BufRead, Seek},
    sync::Arc,
};

use cgmath::{Matrix4, Vector3};
use wavefront_obj::obj::ObjSet;
use image::io::Reader;
use vulkano::device::{Device, Queue};
use wavefront_obj::obj::Object;

use super::static_mesh::{InstancedMesh, StaticMesh};

pub struct Scene {
    static_meshes: Vec<StaticMesh>,
    instanced_meshes: Vec<InstancedMesh>,
    device: Arc<Device>,
    queue: Arc<Queue>,
    world: crate::physics::World,
}

impl Scene {
    pub fn create(device: Arc<Device>, queue: Arc<Queue>) -> Scene {
        Scene {
            static_meshes: vec![],
            instanced_meshes: vec![],
            device,
            queue,
            world: crate::physics::World::new()
        }
    }

    pub fn download_obj_set(path: &Path) -> ObjSet {
        let mut file = match File::open(&path) {
            Err(why) => panic!("couldn't open {}: {}", path.display(), why),
            Ok(file) => file,
        };
        let mut s = String::new();
        match file.read_to_string(&mut s) {
            Err(why) => panic!("couldn't read {}: {}", path.display(), why),
            Ok(_) => (),
        }

        wavefront_obj::obj::parse(s).unwrap()
    }

    pub fn add_object(&mut self, object: &Object, transform: Matrix4<f32>) {
        self.static_meshes.push(StaticMesh::create_from_obj(
            transform,
            object,
            self.device.clone(),
        ));
    }

    pub fn add_instanced_object<R: BufRead + Seek>(
        &mut self,
        object: &Object,
        transforms: Vec<Matrix4<f32>>,
        texture_data_input: R,
    ) {
        let reader = Reader::new(texture_data_input);
        let image = reader
            .with_guessed_format()
            .unwrap()
            .decode()
            .unwrap()
            .to_rgba8();
        let (w, h) = (image.width(), image.height());
        let image_data = image.into_raw().clone();

        self.instanced_meshes.push(InstancedMesh::make(
            transforms.clone(),
            object,
            self.device.clone(),
            self.queue.clone(),
            image_data,
            vulkano::image::ImageDimensions::Dim2d {
                height: h,
                width: w,
                array_layers: 1,
            },
        ));
        for t in transforms {
            self.world.create_rigidbody(Vector3::new(t.w.x, t.w.y, t.w.z), Vector3::new(1.0, 1.0, 1.0));
        }
    }

    pub fn update(&mut self, delta_time: f32) {
        self.world.run_simulation_step(delta_time);
        let mut curr_id = 0;
        /*for instanced_mesh in &mut self.instanced_meshes {
            for t in instanced_mesh.get_transform() {
                self.world.get_rigidbody(curr_id)
            }
        }*/
    }

    pub fn iter_meshes(&self) -> std::slice::Iter<StaticMesh> {
        self.static_meshes.iter()
    }

    pub fn iter_instanced_meshes(&self) -> std::slice::Iter<InstancedMesh> {
        self.instanced_meshes.iter()
    }
}
