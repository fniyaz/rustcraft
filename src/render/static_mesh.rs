use std::sync::Arc;

use cgmath::{Matrix4, SquareMatrix};
use vulkano::{
    buffer::{BufferUsage, CpuAccessibleBuffer},
    device::{Device, Queue},
    format::Format,
    image::{ImageDimensions, ImmutableImage, MipmapsCount},
};
use wavefront_obj::obj::Object;

use super::primitives::{InstanceData, Vertex};

struct ObjParser {}

impl ObjParser {
    fn parse(object: &Object) -> Vec<Vertex> {
        if object.normals.is_empty() {
            ObjParser::parse_and_generate_normals(object)
        } else {
            ObjParser::parse_with_normals(object)
        }
    }

    fn parse_with_normals(object: &Object) -> Vec<Vertex> {
        let mut vertices: Vec<Vertex> = Vec::with_capacity(object.geometry.len() * 3);
        object
            .geometry
            .iter()
            .map(|geometry| geometry.shapes.iter())
            .flatten()
            .for_each(|shape| match shape.primitive {
                wavefront_obj::obj::Primitive::Point(..) => {
                    panic!("Points in objects are unsupported")
                }
                wavefront_obj::obj::Primitive::Line(..) => {
                    panic!("Lines in objects are unsupported")
                }
                wavefront_obj::obj::Primitive::Triangle(idx1, idx2, idx3) => {
                    for idx in vec![idx1, idx2, idx3] {
                        let vert = object.vertices[idx.0];
                        let tex = object.tex_vertices[idx.1.unwrap()];
                        let normal = object.normals[idx.2.unwrap()];
                        vertices.push(Vertex {
                            position: (vert.x as f32, vert.y as f32, vert.z as f32),
                            normal: (normal.x as f32, normal.y as f32, normal.z as f32),
                            uv: (tex.u as f32, 1f32 - tex.v as f32),
                        });
                    }
                }
            });
        vertices
    }

    fn parse_and_generate_normals(object: &Object) -> Vec<Vertex> {
        let mut vertices: Vec<Vertex> = Vec::with_capacity(object.geometry.len() * 3);
        object
            .geometry
            .iter()
            .map(|geometry| geometry.shapes.iter())
            .flatten()
            .for_each(|shape| match shape.primitive {
                wavefront_obj::obj::Primitive::Point(..) => {
                    panic!("Points in objects are unsupported")
                }
                wavefront_obj::obj::Primitive::Line(..) => {
                    panic!("Lines in objects are unsupported")
                }
                wavefront_obj::obj::Primitive::Triangle(idx1, idx2, idx3) => {
                    let v1 = object.vertices[idx1.0];
                    let v2 = object.vertices[idx2.0];
                    let v3 = object.vertices[idx3.0];
                    let u = cgmath::Vector3::new(v2.x, v2.y, v2.z)
                        - cgmath::Vector3::new(v1.x, v1.y, v1.z);
                    let v = cgmath::Vector3::new(v3.x, v3.y, v3.z)
                        - cgmath::Vector3::new(v1.x, v1.y, v1.z);
                    let normal = (
                        (u.y * v.z - u.z * v.y) as f32,
                        (u.z * v.x - u.x * v.z) as f32,
                        (u.x * v.y - u.y * v.x) as f32,
                    );
                    for idx in vec![idx1, idx2, idx3] {
                        let vert = object.vertices[idx.0];
                        let tex = object.tex_vertices[idx.1.unwrap()];
                        vertices.push(Vertex {
                            position: (vert.x as f32, vert.y as f32, vert.z as f32),
                            normal,
                            uv: (tex.u as f32, 1f32 - tex.v as f32),
                        });
                    }
                }
            });
        vertices
    }
}

pub struct StaticMesh {
    transform: Matrix4<f32>,
    pub vertex_buffer: Arc<CpuAccessibleBuffer<[Vertex]>>,
}

impl StaticMesh {
    pub fn create_from_obj(
        transform: Matrix4<f32>,
        object: &Object,
        device: Arc<Device>,
    ) -> StaticMesh {
        let vertex_buffer = CpuAccessibleBuffer::from_iter(
            device.clone(),
            BufferUsage::all(),
            false,
            ObjParser::parse(object).iter().cloned(),
        )
        .unwrap();

        StaticMesh {
            vertex_buffer,
            transform,
        }
    }

    pub fn get_transform(&self) -> Matrix4<f32> {
        self.transform
    }
}

pub struct InstancedMesh {
    pub instance_data_buffer: Arc<CpuAccessibleBuffer<[InstanceData]>>,
    pub vertex_buffer: Arc<CpuAccessibleBuffer<[Vertex]>>,
    pub texture: Arc<ImmutableImage>,
}

impl InstancedMesh {
    pub fn make(
        transforms: Vec<Matrix4<f32>>,
        object: &Object,
        device: Arc<Device>,
        queue: Arc<Queue>,
        texture_data: Vec<u8>,
        dims: ImageDimensions,
    ) -> InstancedMesh {
        let vertex_buffer = CpuAccessibleBuffer::from_iter(
            device.clone(),
            BufferUsage::all(),
            false,
            ObjParser::parse(object).iter().cloned(),
        )
        .unwrap();

        let data: Vec<InstanceData> = transforms
            .iter()
            .map(|t| InstanceData {
                transform: (*t).into(),
            })
            .collect();

        let instance_data_buffer = CpuAccessibleBuffer::from_iter(
            device.clone(),
            BufferUsage::all(),
            false,
            data.into_iter(),
        )
        .unwrap();

        // TODO(Harrm): need to pay attention to this future
        let (texture, _tex_future) = {
            ImmutableImage::from_iter(
                texture_data.iter().cloned(),
                dims,
                MipmapsCount::One,
                Format::R8G8B8A8Srgb,
                queue.clone(),
            )
            .unwrap()
        };

        InstancedMesh {
            vertex_buffer,
            instance_data_buffer,
            texture,
        }
    }

    pub fn get_transform(&self) -> Matrix4<f32> {
        Matrix4::identity()
    }
}
