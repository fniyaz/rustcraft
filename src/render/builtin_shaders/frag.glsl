#version 450

layout(location = 0) in vec3 v_normal;
layout(location = 1) in vec2 v_uv;

layout(location = 0) out vec4 f_color;

layout(set = 1, binding = 0) uniform sampler2D tex;

const vec3 LIGHT = vec3(0.0, 1000.0, 1000.0);

void main() {
    float brightness = dot(normalize(v_normal), normalize(LIGHT));
    vec3 ambient = texture(tex, v_uv).rgb;
    vec3 dark_color = vec3(0.6, 0.6, 0.6);
    vec3 regular_color = vec3(1.0, 1.0, 1.0);

    f_color = vec4(mix(dark_color * ambient, regular_color * ambient, brightness), 1.0);
}
