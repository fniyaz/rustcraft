#version 450

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;


layout(location = 0) out vec4 f_color;

layout(push_constant) uniform PushConstants { 
    vec3 camPos; 
} constants;

vec3 LIGHT = constants.camPos; //vec3(5.0, 0.0, 5.0);
const vec3 LIGHT_COLOR = 100.0 * vec3(1.0, 1.0, 1.0);

const vec3  albedo = vec3(0.3, 0.3, 0.3);
const float metallic = 0.9;
const float roughness = 0.1;

const float PI = 3.14159265359;

/*
The normal distribution function D statistically approximates the relative
surface area of microfacets exactly aligned to the (halfway) vector h.
*/
float DistributionGGX(vec3 N, vec3 H, float a) {
  float a2 = a * a;
  float NdotH = max(dot(N, H), 0.0);
  float NdotH2 = NdotH * NdotH;

  float nom = a2;
  float denom = (NdotH2 * (a2 - 1.0) + 1.0);
  denom = PI * denom * denom;

  return nom / denom;
}

/*
The geometry function statistically approximates the relative surface area where
its micro surface-details overshadow each other, causing light rays to be
occluded.
*/
float GeometrySchlickGGX(float NdotV, float k) {
  float nom = NdotV;
  float denom = NdotV * (1.0 - k) + k;

  return nom / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float k) {
  float NdotV = max(dot(N, V), 0.0);
  float NdotL = max(dot(N, L), 0.0);
  float ggx1 = GeometrySchlickGGX(NdotV, k);
  float ggx2 = GeometrySchlickGGX(NdotL, k);

  return ggx1 * ggx2;
}

/*
The Fresnel equation (pronounced as Freh-nel) describes the ratio of light that
gets reflected over the light that gets refracted, which varies over the angle
we're looking at a surface. The moment light hits a surface, based on the
surface-to-view angle, the Fresnel equation tells us the percentage of light
that gets reflected. From this ratio of reflection and the energy conservation
principle we can directly obtain the refracted portion of light.
*/

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}


void main()
{
    vec3 N = normalize(normal); 
    vec3 V = normalize(constants.camPos - position);

    vec3 L = normalize(LIGHT - position);
    vec3 H = normalize(V + L);
  
    float distance_    = length(LIGHT - position);
    float attenuation = 1.0 / (distance_ * distance_);
    vec3 radiance     = LIGHT_COLOR * attenuation; 

    vec3 F0 = vec3(0.04); 
    F0      = mix(F0, albedo, metallic);
    vec3 F  = fresnelSchlick(max(dot(H, V), 0.0), F0);

    float NDF = DistributionGGX(N, H, roughness);       
    float G   = GeometrySmith(N, V, L, roughness);   

    vec3 numerator    = NDF * G * F;
    float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
    vec3 specular     = numerator / max(denominator, 0.001);  

    vec3 kS = F;
    vec3 kD = vec3(1.0) - kS;
  
    kD *= 1.0 - metallic;	
  
    float NdotL = max(dot(N, L), 0.0);        
    vec3 final_color = (kD * albedo / PI + specular) * radiance * NdotL;

    vec3 ambient = vec3(0.03) * albedo;
    f_color = vec4(final_color + ambient, 1.0);  
}