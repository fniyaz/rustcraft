use vulkano::pipeline::shader::{GraphicsEntryPoint};


pub trait Shader {
    fn main_entry_point(&self) -> GraphicsEntryPoint;
}

pub mod vs {
    vulkano_shaders::shader! {
        ty: "vertex",
        path: "src/render/builtin_shaders/vert.glsl"
    }

    impl super::Shader for Shader {
        fn main_entry_point(&self) -> super::GraphicsEntryPoint {
            self.main_entry_point()
        }
    } 
}

pub mod instanced_vs {
    vulkano_shaders::shader! {
        ty: "vertex",
        path: "src/render/builtin_shaders/instanced_vert.glsl"
    }

    impl super::Shader for Shader {
        fn main_entry_point(&self) -> super::GraphicsEntryPoint {
            self.main_entry_point()
        }
    } 
}
pub mod fs {
    vulkano_shaders::shader! {
        ty: "fragment",
        path: "src/render/builtin_shaders/frag.glsl"
    }

    impl super::Shader for Shader {
        fn main_entry_point(&self) -> super::GraphicsEntryPoint {
            self.main_entry_point()
        }
    } 
}

pub mod pbr_fs {
    vulkano_shaders::shader! {
        ty: "fragment",
        path: "src/render/builtin_shaders/pbr_frag.glsl"
    }

    impl super::Shader for Shader {
        fn main_entry_point(&self) -> super::GraphicsEntryPoint {
            self.main_entry_point()
        }
    } 
}
