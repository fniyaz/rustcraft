use std::sync::Arc;

use vulkano::device::Device;
use vulkano::pipeline::layout::PipelineLayout;
use vulkano::pipeline::vertex::OneVertexOneInstanceDefinition;
use vulkano::pipeline::vertex::SingleBufferDefinition;
use vulkano::render_pass::RenderPass;
use vulkano::{pipeline::GraphicsPipelineAbstract};

use crate::render::builtin_shaders;
use crate::render::pipelines::PipelineBuilder;
use crate::render::pipelines::TrianglePipeline;
use crate::render::primitives::InstanceData;
use crate::render::primitives::Vertex;

struct PipelineData {
    pipeline: Arc<dyn GraphicsPipelineAbstract + Send + Sync>,
    vertex_shader: Box<dyn builtin_shaders::Shader>,
    fragment_shader: Box<dyn builtin_shaders::Shader>,
}

pub struct StaticMeshPipeline {
    data: PipelineData,
}

impl StaticMeshPipeline {
    pub fn create(
        device: Arc<Device>,
        render_pass: Arc<RenderPass>,
        vertex_shader: Box<dyn builtin_shaders::Shader>,
        fragment_shader: Box<dyn builtin_shaders::Shader>,
        dimensions: &[u32; 2],
    ) -> StaticMeshPipeline {
        let pipeline = TrianglePipeline::make(
            device.clone(),
            SingleBufferDefinition::<Vertex>::new(),
            vertex_shader.main_entry_point(),
            fragment_shader.main_entry_point(),
            render_pass.clone(),
            &dimensions,
        );

        StaticMeshPipeline {
            data: PipelineData {
                pipeline,
                vertex_shader,
                fragment_shader,
            },
        }
    }

    pub fn recreate(
        &mut self,
        device: Arc<Device>,
        render_pass: Arc<RenderPass>,
        dimensions: &[u32; 2],
    ) {
        self.data.pipeline = TrianglePipeline::make(
            device.clone(),
            SingleBufferDefinition::<Vertex>::new(),
            self.data.vertex_shader.main_entry_point(),
            self.data.fragment_shader.main_entry_point(),
            render_pass.clone(),
            &dimensions,
        );
    }

    pub fn get_vk_pipeline(&self) -> Arc<dyn GraphicsPipelineAbstract + Send + Sync> {
        self.data.pipeline.clone()
    }

    pub fn get_vk_layout(&self) -> &Arc<PipelineLayout> {
        self.data.pipeline.layout()
    }
}

pub struct InstancedMeshPipeline {
    data: PipelineData,
}

impl InstancedMeshPipeline {
    pub fn create(
        device: Arc<Device>,
        render_pass: Arc<RenderPass>,
        vertex_shader: Box<dyn builtin_shaders::Shader>,
        fragment_shader: Box<dyn builtin_shaders::Shader>,
        dimensions: &[u32; 2],
    ) -> InstancedMeshPipeline {
        let pipeline = TrianglePipeline::make(
            device.clone(),
            OneVertexOneInstanceDefinition::<Vertex, InstanceData>::new(),
            vertex_shader.main_entry_point(),
            fragment_shader.main_entry_point(),
            render_pass.clone(),
            &dimensions,
        );

        InstancedMeshPipeline {
            data: PipelineData {
                pipeline,
                vertex_shader,
                fragment_shader,
            },
        }
    }

    pub fn recreate(
        &mut self,
        device: Arc<Device>,
        render_pass: Arc<RenderPass>,
        dimensions: &[u32; 2],
    ) {
        self.data.pipeline = TrianglePipeline::make(
            device.clone(),
            OneVertexOneInstanceDefinition::<Vertex, InstanceData>::new(),
            self.data.vertex_shader.main_entry_point(),
            self.data.fragment_shader.main_entry_point(),
            render_pass.clone(),
            &dimensions,
        );
    }

    pub fn get_vk_pipeline(&self) -> Arc<dyn GraphicsPipelineAbstract + Send + Sync> {
        self.data.pipeline.clone()
    }

    pub fn get_vk_layout(&self) -> &Arc<PipelineLayout> {
        self.data.pipeline.layout()
    }
}
