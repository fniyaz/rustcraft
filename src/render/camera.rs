use cgmath::{Deg, Euler, InnerSpace, Matrix4, Quaternion, Vector3, Zero};

#[derive(Copy, Clone)]
pub enum CameraMode {
    Orthogonal {
        near: f32,
        far: f32,
        width: u32,
        height: u32,
    },
    Perspective {
        near: f32,
        far: f32,
        fov: f32,
        aspect_ratio: f32,
    },
}

#[derive(Copy, Clone)]
pub struct CameraData {
    mode: CameraMode,
    rotation: Quaternion<f32>,
    translation: Vector3<f32>,
    speed: f32,
}

impl CameraData {
    pub fn new(mode: CameraMode) -> CameraData {
        use cgmath::One;
        CameraData {
            mode,
            rotation: Quaternion::one(),
            translation: Zero::zero(),
            speed: 1.0,
        }
    }
}

pub trait Camera {
    fn add_offset(&mut self, offset: Vector3<f32>);
    fn rotate(&mut self, euler: Euler<Deg<f32>>);

    fn set_speed(&mut self, speed: f32);

    fn set_mode(&mut self, mode: CameraMode);

    fn set_aspect_ratio(&mut self, dimensions: (u32, u32));

    fn get_view_projection_matrix(&self) -> (Matrix4<f32>, Matrix4<f32>);
    fn get_position(&self) -> Vector3<f32>;
}

impl Camera for CameraData {
    fn add_offset(&mut self, offset: Vector3<f32>) {
        use cgmath::Rotation;
        if offset.is_zero() {
            return;
        }
        let dir = self.rotation.rotate_vector(offset.normalize());
        self.translation += offset.magnitude() * self.speed * dir;
    }

    fn rotate(&mut self, euler: Euler<Deg<f32>>) {
        self.rotation = Quaternion::from(euler) * self.rotation;
    }

    fn set_speed(&mut self, speed: f32) {
        self.speed = speed;
    }

    fn set_aspect_ratio(&mut self, dimensions: (u32, u32)) {
        self.mode = match self.mode {
            CameraMode::Perspective {
                near,
                far,
                fov,
                ..
            } => CameraMode::Perspective {
                near,
                far,
                fov,
                aspect_ratio: dimensions.0 as f32 / dimensions.1 as f32,
            },
            CameraMode::Orthogonal {
                far,
                near,
                ..
            } => CameraMode::Orthogonal {
                height: dimensions.1,
                width: dimensions.0,
                far,
                near,
            },
        }
    }

    fn get_view_projection_matrix(&self) -> (Matrix4<f32>, Matrix4<f32>) {
        let proj = match self.mode {
            CameraMode::Perspective {
                near,
                far,
                fov,
                aspect_ratio,
            } => cgmath::perspective(cgmath::Deg(fov), aspect_ratio, near, far),
            CameraMode::Orthogonal {
                near,
                far,
                width,
                height,
            } => cgmath::ortho(
                -(width as f32) / 2.0,
                (width as f32) / 2.0,
                -(height as f32) / 2.0,
                (height as f32) / 2.0,
                near,
                far,
            ),
        };
        use cgmath::Matrix;
        let rot = Matrix4::<f32>::from(self.rotation).transpose();
        let pos = Matrix4::from_translation(-self.translation);
        let view = rot * pos;
        (view, proj)
    }

    fn get_position(&self) -> Vector3<f32> {
        self.translation
    }

    fn set_mode(&mut self, mode: CameraMode) {
        self.mode = mode;
    }
}
