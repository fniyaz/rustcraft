use cgmath::Deg;
use std::{cell::RefCell, rc::Rc, sync::Arc, time::Instant};

use cgmath::{Vector3, Zero};
use winit::window::Window;

use vulkano::{command_buffer::{AutoCommandBufferBuilder, CommandBufferUsage, SubpassContents}, device::{Device, Queue}, format::Format, image::attachment::AttachmentImage, image::view::ImageView, image::{ImageUsage, SwapchainImage}, render_pass::{Framebuffer, FramebufferAbstract, RenderPass}, swapchain, swapchain::{AcquireError, Capabilities, Surface, Swapchain, SwapchainCreationError}, sync, sync::{FlushError, GpuFuture}};

use super::pipelines::{InstancedMeshPipeline, StaticMeshPipeline};
use super::mesh_renderer::{InstancedMeshRenderer, StaticMeshRenderer, MeshRenderer};
use super::scene::Scene;
use super::{
    builtin_shaders::{fs, instanced_vs, vs},
    camera::{Camera, CameraData},
};

pub trait Renderer {
    fn update(&mut self);
    fn resize(&mut self);
    fn on_pressed(&mut self, input: &winit::event::KeyboardInput);
    fn on_released(&mut self, input: &winit::event::KeyboardInput);
}

pub struct RendererData {
    last_update: Instant,
    delta_time: f32,
    movement: Vector3<f32>,
    rotation: f32,

    recreate_swapchain: bool,
    previous_frame_end: Option<Box<dyn GpuFuture>>,
    dimensions: (u32, u32),

    surface: Arc<Surface<Window>>,
    device: Arc<Device>,
    swapchain: Arc<Swapchain<Window>>,

    framebuffers: Vec<Arc<dyn FramebufferAbstract + Send + Sync>>,
    render_pass: Arc<RenderPass>,
    queue: Arc<Queue>,

    scene: Rc<RefCell<Scene>>,
    camera: CameraData,
    static_mesh_renderer: StaticMeshRenderer,
    instanced_mesh_renderer: InstancedMeshRenderer,
}

impl RendererData {
    pub fn create(
        capabilities: Capabilities,
        device: Arc<Device>,
        queue: Arc<Queue>,
        surface: Arc<Surface<Window>>,
        scene: Rc<RefCell<Scene>>,
    ) -> RendererData {
        let dimensions: [u32; 2] = surface.window().inner_size().into();
        let (swapchain, images) = {
            let caps = capabilities;
            let format = caps.supported_formats[0].0;
            let composite_alpha = caps.supported_composite_alpha.iter().next().unwrap();
            let dimensions: [u32; 2] = surface.window().inner_size().into();
            Swapchain::start(device.clone(), surface.clone())
                .num_images(caps.min_image_count)
                .format(format)
                .dimensions(dimensions)
                .usage(ImageUsage::color_attachment())
                .sharing_mode(&queue)
                .composite_alpha(composite_alpha)
                .build()
                .unwrap()
        };
        let render_pass = Arc::new(
            vulkano::single_pass_renderpass!(device.clone(),
                attachments: {
                    color: {
                        load: Clear,
                        store: Store,
                        format: swapchain.format(),
                        samples: 1,
                    },
                    depth: {
                        load: Clear,
                        store: DontCare,
                        format: Format::D16Unorm,
                        samples: 1,
                    }
                },
                pass: {
                    color: [color],
                    depth_stencil: {depth}
                }
            )
            .unwrap(),
        );
        let framebuffers =
            Self::window_size_dependent_setup(device.clone(), render_pass.clone(), &images);
        let mut camera = CameraData::new(crate::render::camera::CameraMode::Perspective {
            near: 0.1,
            far: 10000.0,
            fov: 60.0,
            aspect_ratio: dimensions[0] as f32 / dimensions[1] as f32,
        });
        camera.add_offset(Vector3::<f32>::new(0.0, 0.0, 10.0));
        camera.set_speed(10.0);

        let vs = vs::Shader::load(device.clone()).unwrap();
        let fs_1 = fs::Shader::load(device.clone()).unwrap();
        let inst_vs = instanced_vs::Shader::load(device.clone()).unwrap();
        let fs_2 = fs::Shader::load(device.clone()).unwrap();

        let static_mesh_renderer = StaticMeshRenderer::make(
            device.clone(),
            camera,
            StaticMeshPipeline::create(
                device.clone(),
                render_pass.clone(),
                Box::new(vs),
                Box::new(fs_1),
                &dimensions,
            ),
        );

        let instanced_mesh_renderer = InstancedMeshRenderer::make(
            device.clone(),
            camera,
            InstancedMeshPipeline::create(
                device.clone(),
                render_pass.clone(),
                Box::new(inst_vs),
                Box::new(fs_2),
                &dimensions,
            ),
        );

        RendererData {
            last_update: Instant::now(),
            delta_time: 0.0,
            movement: Vector3::zero(),
            rotation: 0.0,

            recreate_swapchain: false,
            previous_frame_end: Some(sync::now(device.clone()).boxed()),
            scene,
            surface,
            swapchain,
            device,
            dimensions: (dimensions[0], dimensions[1]),
            framebuffers,
            render_pass,
            queue,
            camera,
            static_mesh_renderer,
            instanced_mesh_renderer,
        }
    }

    fn recreate_swapchain(&mut self) {
        let dimensions: [u32; 2] = self.surface.window().inner_size().into();
        let (new_swapchain, new_images) =
            match self.swapchain.recreate().dimensions(dimensions).build() {
                Ok(r) => r,
                Err(SwapchainCreationError::UnsupportedDimensions) => return,
                Err(e) => panic!("Failed to recreate swapchain: {:?}", e),
            };

        self.swapchain = new_swapchain;
        let new_framebuffers = Self::window_size_dependent_setup(
            self.device.clone(),
            self.render_pass.clone(),
            &new_images,
        );
        self.static_mesh_renderer.recreate_pipeline(
            self.device.clone(),
            self.render_pass.clone(),
            &dimensions,
        );
        self.instanced_mesh_renderer.recreate_pipeline(
            self.device.clone(),
            self.render_pass.clone(),
            &dimensions,
        );
        self.framebuffers = new_framebuffers;
        self.dimensions = (dimensions[0], dimensions[1]);
        self.camera.set_aspect_ratio(self.dimensions);
        self.recreate_swapchain = false;
    }

    fn window_size_dependent_setup(
        device: Arc<Device>,
        render_pass: Arc<RenderPass>,
        images: &[Arc<SwapchainImage<Window>>],
    ) -> Vec<Arc<dyn FramebufferAbstract + Send + Sync>> {
        let dimensions = images[0].dimensions();
        let depth_buffer = ImageView::new(
            AttachmentImage::transient(device.clone(), dimensions, Format::D16Unorm).unwrap(),
        )
        .unwrap();
        let framebuffers = images
            .iter()
            .map(|image| {
                let view = ImageView::new(image.clone()).unwrap();
                Arc::new(
                    Framebuffer::start(render_pass.clone())
                        .add(view)
                        .unwrap()
                        .add(depth_buffer.clone())
                        .unwrap()
                        .build()
                        .unwrap(),
                ) as Arc<dyn FramebufferAbstract + Send + Sync>
            })
            .collect::<Vec<_>>();
        framebuffers
    }
}

impl Renderer for RendererData {
    fn update(&mut self) {
        self.previous_frame_end.as_mut().unwrap().cleanup_finished();

        if self.recreate_swapchain {
            self.recreate_swapchain();
        }

        self.static_mesh_renderer.update_camera_data(self.camera);
        self.instanced_mesh_renderer.update_camera_data(self.camera);
        let (image_num, suboptimal, acquire_future) =
            match swapchain::acquire_next_image(self.swapchain.clone(), None) {
                Ok(r) => r,
                Err(AcquireError::OutOfDate) => {
                    self.recreate_swapchain = true;
                    return;
                }
                Err(e) => panic!("Failed to acquire next image: {:?}", e),
            };

        if suboptimal {
            self.recreate_swapchain = true;
        }

        let mut builder = AutoCommandBufferBuilder::primary(
            self.device.clone(),
            self.queue.family(),
            CommandBufferUsage::OneTimeSubmit,
        )
        .unwrap();
        builder
            .begin_render_pass(
                self.framebuffers[image_num].clone(),
                SubpassContents::Inline,
                vec![[0.5, 0.5, 0.8, 1.0].into(), 1f32.into()],
            )
            .unwrap();
        for mesh in self.scene.borrow().iter_meshes() {
            self.static_mesh_renderer
                .render_mesh(&mut builder, mesh);
        }
        for mesh in self.scene.borrow().iter_instanced_meshes() {
            self.instanced_mesh_renderer
                .render_mesh(&mut builder, mesh);
        }
        builder.end_render_pass().unwrap();
        let command_buffer = builder.build().unwrap();

        let future = self
            .previous_frame_end
            .take()
            .unwrap()
            .join(acquire_future)
            .then_execute(self.queue.clone(), command_buffer)
            .unwrap()
            .then_swapchain_present(self.queue.clone(), self.swapchain.clone(), image_num)
            .then_signal_fence_and_flush();

        match future {
            Ok(future) => {
                self.previous_frame_end = Some(future.boxed());
            }
            Err(FlushError::OutOfDate) => {
                self.recreate_swapchain = true;
                self.previous_frame_end = Some(sync::now(self.device.clone()).boxed());
            }
            Err(e) => {
                println!("Failed to flush future: {:?}", e);
                self.previous_frame_end = Some(sync::now(self.device.clone()).boxed());
            }
        }
        self.delta_time = (Instant::now() - self.last_update).as_secs_f32();
        self.last_update = Instant::now();
        self.camera.add_offset(self.movement * self.delta_time);
        self.camera.rotate(cgmath::Euler::new(
            Deg(0.0),
            Deg(self.rotation * self.delta_time),
            Deg(0.0),
        ));
    }

    fn resize(&mut self) {
        self.recreate_swapchain = true;
    }

    fn on_pressed(&mut self, input: &winit::event::KeyboardInput) {
        match input.virtual_keycode {
            Some(winit::event::VirtualKeyCode::W) => self.movement.z = -1.0,
            Some(winit::event::VirtualKeyCode::S) => self.movement.z = 1.0,
            Some(winit::event::VirtualKeyCode::D) => self.movement.x = 1.0,
            Some(winit::event::VirtualKeyCode::A) => self.movement.x = -1.0,
            Some(winit::event::VirtualKeyCode::Q) => self.rotation = 90.0,
            Some(winit::event::VirtualKeyCode::E) => self.rotation = -90.0,
            Some(..) => {}
            None => {}
        }
    }

    fn on_released(&mut self, input: &winit::event::KeyboardInput) {
        match input.virtual_keycode {
            Some(winit::event::VirtualKeyCode::W) => self.movement.z = 0.0,
            Some(winit::event::VirtualKeyCode::S) => self.movement.z = 0.0,
            Some(winit::event::VirtualKeyCode::D) => self.movement.x = 0.0,
            Some(winit::event::VirtualKeyCode::A) => self.movement.x = 0.0,
            Some(winit::event::VirtualKeyCode::Q) => self.rotation = 0.0,
            Some(winit::event::VirtualKeyCode::E) => self.rotation = 0.0,
            Some(..) => {}
            None => {}
        }
    }
}
