
struct Block {
}

pub struct WorldDescription {
    pub width: u32,
    pub length: u32,
    pub height: u32,
    blocks: Vec<Block>,
}

pub enum WorldDescriptionError {
    SizeMismatch
}

impl WorldDescription {
    pub fn create_from_iter(width, length, height: u32, iter: impl Iterator<Item = Block>) -> Result<WorldDescription, WorldDescriptionError> {
        let wd = WorldDescription {
            width, length, height,
            blocks: iter.collect()
        };
        if wd.blocks.len() == width * length * height && wd.blocks.len() % width == 0 && wd.blocks.len() % height == 0 && wd.blocks.len() % length == 0 {
            Ok(wd)
        }
        else {
            Err(WorldDescriptionError::SizeMismatch)
        } 
    }

    pub fn get_block(&self, x: u32, y: u32, z: u32) -> &Block {
        &self.blocks[(z * self.length * self.height + y * self.width + x) as usize]
    } 
}

pub struct WorldGenerator {}

impl WorldGenerator {

    fn generate() -> WorldDescription {
        rand::rngs::SmallRng::from_seed(seed: Self::Seed)
    }

}