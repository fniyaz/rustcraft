
mod render;
mod physics;
//mod pcg;

use crate::render::VulkanApplication;

fn main() {
    let app = VulkanApplication::create();
    app.run();
}
