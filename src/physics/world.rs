use cgmath::{Vector3, Zero};

use super::collider::AabbCollider;

#[derive(Copy, Clone)]
struct RigidBody {
    id: u32,
    collider: AabbCollider,
    mass: f32,
    net_force: Vector3<f32>,
    velocity: Vector3<f32>,
}

pub struct World {
    bodies: Vec<RigidBody>,
    gravity: Vector3<f32>,
}

impl World {
    pub fn new() -> World {
        World {
            bodies: Vec::new(),
            gravity: Vector3::new(0.0, -9.8, 0.0)
        }
    }

    pub fn create_rigidbody(&mut self, pos: Vector3<f32>, size: Vector3<f32>) -> u32 {
        let rb = RigidBody {
            id: self.bodies.last().map_or(0, |b| b.id + 1),
            collider: AabbCollider::create(pos, size),
            mass: size.x * size.y * size.z,
            net_force: self.gravity,
            velocity: Vector3::zero(),
        };
        let id = rb.id;
        self.bodies.push(rb);
        id
    }

    pub fn get_rigidbody(&self, id: u32) -> &RigidBody {
        &self.bodies[id as usize]
    }

    pub fn run_simulation_step(&mut self, delta_time: f32) {
        let mut collisions = Vec::new();

        for body in &mut self.bodies {
            body.velocity += body.net_force * delta_time;
            body.collider.add_offset(body.velocity);
        }

        for body in &self.bodies {
            for other_body in self.bodies.iter().cloned() {
                if other_body.id != body.id && body.collider.does_intersect(&other_body.collider) {
                    collisions.push((body.id, other_body.id));
                }
            }
        }
        for (a, b) in collisions {
            let b_clone = self.bodies[b as usize].collider.clone();
            self.bodies[a as usize]
                .collider
                .resolve_collision(&b_clone);
        }
    }
}
