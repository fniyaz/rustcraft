use cgmath::{InnerSpace, Vector3, Zero};

#[derive(Copy, Clone)]
pub struct AabbCollider {
    min: Vector3<f32>,
    max: Vector3<f32>,
}

fn minmax<T: Copy + PartialOrd>(a: T, b: T) -> (T, T) {
    if a < b {
        (a, b)
    } else {
        (b, a)
    }
}

impl AabbCollider {
    pub fn create(pos: Vector3<f32>, size: Vector3<f32>) -> AabbCollider {
        let (min_x, max_x) = minmax(pos.x, size.x);
        let (min_y, max_y) = minmax(pos.y, size.y);
        let (min_z, max_z) = minmax(pos.z, size.z);
        let min = Vector3::new(min_x, min_y, min_z);
        let max = Vector3::new(max_x, max_y, max_z);
        AabbCollider { min, max }
    }

    pub fn add_offset(&mut self, offset: Vector3<f32>) {
        self.min += offset;
        self.max += offset;
    }

    pub fn does_intersect(&self, b: &AabbCollider) -> bool {
        return (self.min.x <= b.max.x && self.max.x >= b.min.x)
            && (self.min.y <= b.max.y && self.max.y >= b.min.y)
            && (self.min.z <= b.max.z && self.max.z >= b.min.z);
    }

    /**
     * @returns amount of movement which will lead to collision
     */
    pub fn resolve_collision(&mut self, b: &AabbCollider) {
        if self.does_intersect(b) {
            // faces in YZ plane, right and left
            let yz_r2l = self.max.x - b.min.x;
            let yz_l2r = -(self.min.x - b.max.x);
            // faces in XY plane, front and back
            let xy_f2b = self.min.z - b.max.z;
            let xy_b2f = -(self.max.z - b.min.z);
            // faces in XZ plane, top and bottom
            let xz_t2b = self.max.y - b.min.y;
            let xz_b2t = -(self.min.y - b.max.y);

            // find the least penetrating edge and push out
            let min_face = *vec![yz_l2r, yz_r2l, xy_b2f, xy_f2b, xz_b2t, xz_t2b]
                .iter()
                .min_by(|a, b| a.partial_cmp(b).unwrap_or(std::cmp::Ordering::Equal))
                .unwrap();
            if min_face == yz_r2l {
                self.add_offset(Vector3::new(-yz_r2l, 0.0, 0.0));
            } else if min_face == yz_l2r {
                self.add_offset(Vector3::new(yz_l2r, 0.0, 0.0));
            } else if min_face == xy_f2b {
                self.add_offset(Vector3::new(0.0, 0.0, -xy_f2b));
            } else if min_face == xy_b2f {
                self.add_offset(Vector3::new(0.0, 0.0, xy_b2f));
            } else if min_face == xz_t2b {
                self.add_offset(Vector3::new(0.0, -xz_t2b, 0.0));
            } else if min_face == xz_b2t {
                self.add_offset(Vector3::new(0.0, xz_b2t, 0.0));
            } else {
                unreachable!()
            }
        }
    }
}
